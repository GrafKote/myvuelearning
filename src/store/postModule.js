import axios from "axios";


export const postModule = {
    state: () => ({
        posts: [],
        isPostsLoading: false,
        selectedSort: '',
        searchQuery: '',
        page: 1,
        limit: 10,
        totalPages: 0,
        sortOptions: [
            {value: 'title', name: 'По названию'},
            {value: 'body', name: 'По описанию'}
        ]
    }),
    getters: {
        sortedPosts(state) {
            return [...state.posts].sort((post1, post2) => post1[state.selectedSort]?.localeCompare(post2[state.selectedSort]))
        },
        sortedAndSearchedPosts(state, getters) {
            return getters.sortedPosts.filter(post => post.title.toLowerCase().includes(state.searchQuery.toLowerCase()))
        }
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        },
        setPage(state, page) {
          state.page = page
        },
        setLoading(state, bool) {
            state.isPostLoading = bool
        },
        setSelectedSort(state, selectedSort) {
            state.selectedSort = selectedSort
        },
        setSearchQuery(state, searchQuery) {
            state.searchQuery = searchQuery
        },
        setTotalPages(state, totalPages) {
            state.totalPages = totalPages
        }
    },
    actions: {
        async getPostFromApi(contex){
            console.log('start fetch')
            try {
                contex.commit('setLoading', true)
                const response = await axios.get('https://jsonplaceholder.typicode.com/posts', {
                    params: {
                        _limit: contex.state.limit,
                        _page: contex.state.page,
                    }
                })
                contex.commit('setTotalPages', Math.ceil(response.headers['x-total-count'] / contex.state.limit))
                contex.commit('setPosts', response.data)
            } catch (e){
                alert('Ошибка')
            } finally {
                contex.commit('setLoading', false)
            }
        },
        async getMorePostFromApi(contex){
            contex.commit('setPage', contex.state.page + 1)
            try {
                const response = await axios.get('https://jsonplaceholder.typicode.com/posts', {
                    params: {
                        _limit: contex.state.limit,
                        _page: contex.state.page,
                    }
                })

                contex.commit('setTotalPages', Math.ceil(response.headers['x-total-count'] / contex.state.limit))
                contex.commit('setPosts', [...contex.state.posts, ...response.data])
            } catch (e) {
                alert('Ошибка')
            }
        }
    },
    namespaced: true
}