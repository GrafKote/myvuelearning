import {createRouter, createWebHashHistory} from "vue-router";
import Main from "@/pages/Main";
import PostPage from "@/pages/PostPage";
import About from "@/pages/About";
import PostPageID from "@/pages/PostPageID";
import PostPageGlobal from "@/pages/PostPageGlobal";
import PostPageCompositionApi from "@/pages/PostPageCompositionApi"

const routes = [
    {
        path: '/',
        component: Main
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/posts',
        component: PostPage
    },
    {
        path: '/posts/:id',
        component: PostPageID
    },
    {
        path: '/store',
        component: PostPageGlobal
    },
    {
        path: '/composition',
        component: PostPageCompositionApi
    }
]

const router = createRouter({
    routes,
    history: createWebHashHistory(process.env.BASE_URL)
})


export default router